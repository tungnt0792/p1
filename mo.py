import json
import random
import time

from kafka import KafkaConsumer, KafkaProducer
from constants import TOPIC_IN_NAME, TOPIC_OUT_NAME, BROKER_URLS
# logging.basicConfig(level=logging.DEBUG)


statusdict = {
    "-4": "Permission denied",
    "-3": "Smpp session is not available",
    "-2": "Client ID is not valid",
    "-1": "Connect to SMPP Host failed or Bind failed",
    "0": "Success",
    "1": "Invalid Message Length (sm_length parameter)",
    "2": "Invalid Command Length (command_length in SMPP PDU)",
    "3": "Invalid Command ID (command_id in SMPP PDU)",
    "4": "Incorrect BIND status for given command",
    "5": "ESME already in bound state",
    "6": "Invalid Priority Flag (priority_flag parameter)",
    "7": "Invalid Regstered Delivery Flag (registered_delivery parameter)",
    "8": "System Error (indicates server problems on the SMPP host)",
    "0x0A": "Invalid source address (sender/source address is not valid)",
    "0x0B": "Invalid desintation address (recipient/destination phone number is not valid)",
    "0x0C": "Message ID is invalid (error only relevant to query_sm, replace_sm, cancel_sm commands)",
    "0x0D": "Bind failed (login/bind failed – invalid login credentials or login restricted by IP address)",
    "0x0E": "Invalid password",
    "0x0F": "Invalid System ID (login/bind failed – invalid username / system id)",
    "0x11": "cancel_sm request failed",
    "0x13": "replace_sm request failed",
    "0x14": "Message Queue Full (This can indicate that the SMPP server has too many queued messages and temporarily cannot accept any more messages. It can also indicate that the SMPP server has too many messages pending for the specified recipient and will not accept any more messages for this recipient until it is able to deliver messages that are already in the queue to this recipient.)",
    "0x15": "Invalid service_type value",
    "0x33": "Invalid number_of_dests value in submit_multi request",
    "0x34": "Invalid distribution list name in submit_multi request",
    "0x40": "Invalid dest_flag in submit_multi request",
    "0x42": "Invalid ‘submit with replace’ request (replace_if_present flag set)",
    "0x43": "Invalid esm_class field data",
    "0x44": "Cannot submit to distribution list (submit_multi request)",
    "0x45": "Submit message failed",
    "0x48": "Invalid Source address TON",
    "0x49": "Invalid Source address NPI",
    "0x50": "Invalid Destination address TON",
    "0x51": "Invalid Destination address NPI",
    "0x53": "Invalid system_type field",
    "0x54": "Invalid replace_if_present flag",
    "0x55": "Invalid number_of_messages parameter",
    "0x58": "Throttling error (This indicates that you are submitting messages at a rate that is faster than the provider allows)",
    "0x61": "Invalid schedule_delivery_time parameter",
    "0x62": "Invalid validity_period parameter / Expiry time",
    "0x63": "Invalid sm_default_msg_id parameter (this error can sometimes occur if the “Default Sender Address” field is blank in SMSCCENTER)",
    "0x64": "ESME Receiver Temporary App Error Code",
    "0x65": "ESME Receiver Permanent App Error Code (the SMPP provider is rejecting the message due to a policy decision or message filter)",
    "0x66": "ESME Receiver Reject Message Error Code (the SMPP provider is rejecting the message due to a policy decision or message filter)",
    "0x67": "query_sm request failed",
    "0xC0": "Error in the optional TLV parameter encoding",
    "0xC1": "An optional TLV parameter was specified which is not allowed",
    "0xC2": "An optional TLV parameter has an invalid parameter length",
    "0xC3": "An expected optional TLV parameter is missing",
    "0xC4": "An optional TLV parameter is encoded with an invalid value",
    "0xFE": "Generice Message Delivery failure",
    "0xFF": "An unknown error occurred (indicates server problems on the SMPP host)"
}


def generate_mo(message):
    data = message.value
    randict = {}
    statuscode = random.choice(list(statusdict.items()))
    randict['eventId'] = random.randrange(3)
    randict['smsId'] = data['smsId']
    randict['isdn'] = data['isdn']
    randict['error'] = statuscode[1]
    randict['status'] = statuscode[0]
    randict['timestamp'] = message[3]
    return randict


def main():
    producer = KafkaProducer(
        bootstrap_servers=BROKER_URLS,
        value_serializer=lambda m: json.dumps(m).encode('utf-8')
    )
    while True:
        consumer = KafkaConsumer(
            TOPIC_IN_NAME,
            bootstrap_servers=BROKER_URLS,
            enable_auto_commit=True,
            auto_offset_reset='latest',
            consumer_timeout_ms=5000,
            value_deserializer=lambda m: json.loads(m.decode('utf-8'))
        )
        for message in consumer:
            mt = generate_mo(message)
            producer.send(TOPIC_OUT_NAME, mt)
            producer.flush()
        time.sleep(1)


if __name__ == '__main__':
    main()
