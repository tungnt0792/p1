import json

from kafka import KafkaProducer

from constants import TOPIC_IN_NAME, TOPIC_OUT_NAME, BROKER_URLS


def main():
    producer = KafkaProducer(
        bootstrap_servers=BROKER_URLS,
        value_serializer=lambda m: json.dumps(m).encode('utf-8')
    )
    msg = {
        'smsId': 2114,
        'clientId': 4,
        'isdn': 'tBM2mPAU/AmNEf7ruB6baQ==',
        'message': 'hello world 01',
        'sourceAddress': None,
        'isBrandname': 0
    }
    producer.send(TOPIC_IN_NAME, msg)
    producer.flush()


if __name__ == '__main__':
    main()
